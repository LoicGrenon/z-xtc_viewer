# -*- coding: utf-8 -*-

import logging
import time
from PySide import QtCore


class DeviceWrapper(QtCore.QObject):

    reply = QtCore.Signal(object)

    def __init__(self,
                 device=None,
                 poll_interval=0.1):
        super(DeviceWrapper, self).__init__()
        self._is_exiting = False
        self._poll_interval = poll_interval

        self.logger = logging.getLogger("app.DeviceWrapper")
        self.set_device(device)

    def __del__(self):
        self._is_exiting = True
        self._close()

    def _close(self):
        if self.device:
            self.device.modbus.serial.close()

    def run(self):
        self.logger.debug("Starting")
        self._is_exiting = False
        while not self._is_exiting:
            self.reply.emit(self.device.get_channels_value())
            time.sleep(self._poll_interval)
        self.logger.debug("Stopping")
        self._close()

    def stop(self):
        self._is_exiting = True

    def set_device(self, device):
        self.device = device

    def set_poll_interval(self, interval):
        self._poll_interval = interval
