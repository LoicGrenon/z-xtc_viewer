# -*- coding: utf-8 -*-

import csv
import logging
import pyqtgraph as pg
import sys
import time
from random import randint
from serial import SerialException
from serial.tools import list_ports
from PySide import QtCore, QtGui

from DeviceWrapper import DeviceWrapper
from ui.main import Ui_MainWindow
from Z_4TC import Z_4TC


class ZxTC_Viewer(QtGui.QMainWindow):
    def __init__(self):
        super(ZxTC_Viewer, self).__init__()

        self.logger = logging.getLogger("app")
        self._setup_logger()

        self.ui = Ui_MainWindow()
        self._setup_Ui()

        self.fill_serial_ports_list()
        self.samples = []
        self._channels = [{'id': 1, 'active': True, 'color': None},
                          {'id': 2, 'active': True, 'color': None},
                          {'id': 3, 'active': True, 'color': None},
                          {'id': 4, 'active': True, 'color': None}]
        self._store_plot_value = True
        self._start_T = time.clock()

        self._device_wrapper = DeviceWrapper()
        self._device_wrapper.reply.connect(self.on_device_wrapper_reply)

        self.thread = QtCore.QThread()
        self.thread.started.connect(self._device_wrapper.run)

        self._plot_refresh_timer = QtCore.QTimer()
        self._plot_refresh_timer.timeout.connect(lambda: self._updatePlot(clear=False))

    def _setup_Ui(self):
        self.ui.setupUi(self)
        self.ui.serialDisconnect_btn.setEnabled(False)

        self.ui.serialPortListRefresh_btn.clicked.connect(self.fill_serial_ports_list)
        self.ui.serialConnect_btn.clicked.connect(self.on_serial_connect)
        self.ui.serialDisconnect_btn.clicked.connect(self.on_serial_disconnect)

        self.ui.actionLoad.triggered.connect(self._file_load)
        self.ui.actionSave_As.triggered.connect(self._file_save_as)
        self.ui.actionQuit.triggered.connect(self.closeEvent)

        self.ui.ch1_active_chkbox.stateChanged.connect(lambda state: self._chg_channel_active_state(1, state))
        self.ui.ch2_active_chkbox.stateChanged.connect(lambda state: self._chg_channel_active_state(2, state))
        self.ui.ch3_active_chkbox.stateChanged.connect(lambda state: self._chg_channel_active_state(3, state))
        self.ui.ch4_active_chkbox.stateChanged.connect(lambda state: self._chg_channel_active_state(4, state))

        self.ui.ch1_toolbtn.clicked.connect(lambda: self._on_channel_toolbtn_click(1))
        self.ui.ch2_toolbtn.clicked.connect(lambda: self._on_channel_toolbtn_click(2))
        self.ui.ch3_toolbtn.clicked.connect(lambda: self._on_channel_toolbtn_click(3))
        self.ui.ch4_toolbtn.clicked.connect(lambda: self._on_channel_toolbtn_click(4))

    def _setup_logger(self):
        ch = logging.StreamHandler()
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(name)s - %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)
        self.logger.setLevel(logging.DEBUG)

    def closeEvent(self, event):
        """
        Reimplementation of QWidget.closeEvent()
        This event handler is called with the given event when Qt receives a window close request
        for a top-level widget from the window system.
        :param event: QCloseEvent
        """
        self.on_serial_disconnect()

    def fill_serial_ports_list(self):
        """
        Get the list of serial ports existing on the computer and fill the QComboBox
        Enable the "Connect" button if the list is not empty
        """
        self.ui.serialPortsList_cb.clear()
        available_ports = list_ports.comports()
        for port in available_ports:
            self.ui.serialPortsList_cb.addItem(port.device)

        self.ui.serialConnect_btn.setEnabled(not len(available_ports) == 0)

    def _enable_serial_buttons(self, state=True):
        self.ui.serialConnect_btn.setEnabled(state)
        self.ui.serialDisconnect_btn.setEnabled(not state)
        self.ui.serialPortsList_cb.setEnabled(state)
        self.ui.serialPortListRefresh_btn.setEnabled(state)

    def on_serial_connect(self):
        """
        Function called when the user clicks on the "Connect" button
        Connect to the selected serial port and read data
        """
        port = self.ui.serialPortsList_cb.currentText()
        try:
            self._device_wrapper.set_device(Z_4TC(port))
        except SerialException as e:
            QtGui.QMessageBox.critical(self.ui.centralwidget,
                                           u"COM port error",
                                           f"An error occurred while trying to open serial port {port} :\n{e}",
                                           QtGui.QMessageBox.Ok)
            return
        self._device_wrapper.moveToThread(self.thread)
        self.thread.start()

        updateFreq = 1
        self._plot_refresh_timer.start(1000.0 * updateFreq)
        self._start_T = time.clock()

        self.samples = [[], []]
        self._store_plot_value = True
        self._updatePlot(clear=True)

        self._enable_serial_buttons(False)

    def on_serial_disconnect(self):
        """
        Function called when the user clicks on the "Disconnect" button.
        Disconnect from serial port and release it
        """
        self._plot_refresh_timer.stop()
        self._device_wrapper.stop()
        self.thread.quit()
        self.thread.wait()

        self._enable_serial_buttons()

    def _add_samples_values(self, values, scaling=1.0):
        for channel_id, value in enumerate(values, 1):
            channel_exists = False
            y_val = float(value) * scaling
            for sample in self.samples[1]:
                # Add data to existing channel
                if sample['name'] == channel_id:
                    channel_exists = True
                    sample['y'].append(y_val)
                    break
            # Add channel if it doesn't exists yet ...
            if not channel_exists:
                color = (randint(0, 255), randint(1, 255))  # (int, hues)
                self.samples[1].append({'name': channel_id, 'color': color, 'y': [y_val]})

    def on_device_wrapper_reply(self, reply):
        # Prevent to store data till the plot isn't updated yet
        if self._plot_refresh_timer.isActive() and self._store_plot_value:
            self._store_plot_value = False
            x_val = round(time.clock() - self._start_T, 2)
            self.samples[0].append(x_val)
            self._add_samples_values(reply, scaling=0.1)

    def _get_channel_config(self, channel_id):
        # Get the dict related to channel_id from self._channels list ...
        return next((item for item in self._channels if item['id'] == channel_id), None)

    def _updatePlot(self, clear=True):
        self._store_plot_value = True  # Plot is updated so new data can be stored
        if clear:
            self.ui.plot_view.clear()
            self._add_cross_hair_on_plot()

        for sample in self.samples[1]:
            channel_config = self._get_channel_config(sample['name'])
            color = channel_config['color'] if channel_config['color'] else sample['color']
            if channel_config['active']:
                item = pg.PlotDataItem({'x': self.samples[0], 'y': sample['y']}, pen=color)
                self.ui.plot_view.addItem(item)
        self.ui.plot_view.repaint()

    def _file_save_as(self):
        # TODO: stop any running recording ?
        name, _filter = QtGui.QFileDialog.getSaveFileName(self, 'Save File', filter="CSV (*.csv)")
        if not name:
            return False

        with open(name, "w", newline='') as csv_file:
            writer = csv.writer(csv_file)
            for i, val in enumerate(self.samples[0]):
                row = [val]
                row.extend([channel['y'][i] for channel in self.samples[1]])
                writer.writerow(row)
        return True

    def _file_load(self):
        # Stop any running recording
        self.on_serial_disconnect()

        name, _filter = QtGui.QFileDialog.getOpenFileName(self, 'Open File', filter="CSV (*.csv)")
        if not name:
            return False

        self.samples = [[], []]
        with open(name, "r", newline='') as csv_file:
            reader = csv.reader(csv_file)
            for row in reader:
                try:
                    self.samples[0].append(float(row[0]))
                    self._add_samples_values(row[1:])
                except (KeyError, ValueError):
                    self.logger.error("Unable to load the CSV file")
                    return False
        self._updatePlot()
        return True

    def _chg_channel_active_state(self, channel_id, channel_state):
        channel_config = self._get_channel_config(channel_id)
        channel_config['active'] = channel_state == QtCore.Qt.Checked

        self._updatePlot()

    def _on_channel_toolbtn_click(self, channel_id):
        color = QtGui.QColorDialog.getColor()
        if color != QtGui.QColor():
            channel_config = self._get_channel_config(channel_id)
            channel_config['color'] = color

            self._updatePlot()

    def _add_cross_hair_on_plot(self):
        plot_view = self.ui.plot_view
        vLine = pg.InfiniteLine(angle=90, movable=False)
        hLine = pg.InfiniteLine(angle=0, movable=False)
        plot_view.addItem(vLine, ignoreBounds=True)
        plot_view.addItem(hLine, ignoreBounds=True)

        vb = plot_view.plotItem.vb

        # Slot triggered on mouse move event over the plot widget
        def mouseMoved(evt):
            pos = evt[0]  # using signal proxy turns original arguments into a tuple
            mousePoint = vb.mapSceneToView(pos)
            vLine.setPos(mousePoint.x())
            hLine.setPos(mousePoint.y())
            self.ui.statusbar.showMessage(f"({mousePoint.x():.2f}; {mousePoint.y():.2f})")

        # signal proxy need to be a class member
        self.plotSigProxy = pg.SignalProxy(plot_view.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)

    w = ZxTC_Viewer()
    w.show()
    sys.exit(app.exec_())
