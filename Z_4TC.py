# -*- coding: utf-8 -*-

import minimalmodbus


ADR_MACHINE_ID = 0
ADR_BAUDRATE = 1
ADR_EPRFLAG = 2
ADR_INPUT_1_TYPE = 3
ADR_INPUT_2_TYPE = 4
ADR_INPUT_3_TYPE = 5
ADR_INPUT_4_TYPE = 6
ADR_INPUT_1_FILTER = 7
ADR_INPUT_2_FILTER = 8
ADR_INPUT_3_FILTER = 9
ADR_INPUT_4_FILTER = 10
ADR_STATUS = 11
ADR_CH1_VAL = 12
ADR_CH2_VAL = 13
ADR_CH3_VAL = 14
ADR_CH4_VAL = 15
ADR_FW_REV = 16
ADR_ERRORS = 18
ADR_CH1_FLOAT = 26
ADR_CH2_FLOAT = 28
ADR_CH3_FLOAT = 30
ADR_CH4_FLOAT = 32


class Z_4TC:

    MAX_CHANNEL = 4

    def __init__(self,
                 serial_port,
                 modbus_address=1,
                 serial_baudrate=2400,
                 serial_bytesize=8,
                 serial_parity=minimalmodbus.serial.PARITY_NONE,
                 serial_stopbits=1,
                 timeout=1):
        self.modbus = minimalmodbus.Instrument(serial_port, modbus_address)
        self.modbus.serial.baudrate = serial_baudrate
        self.modbus.serial.bytesize = serial_bytesize
        self.modbus.serial.parity = serial_parity
        self.modbus.serial.stopbits = serial_stopbits
        self.modbus.serial.timeout = timeout
        self.modbus.mode = minimalmodbus.MODE_RTU

    def read_register(self, *args, **kwargs):
        return self.modbus.read_register(*args, **kwargs)

    def read_registers(self, *args, **kwargs):
        return self.modbus.read_registers(*args, **kwargs)

    def write_register(self, *args, **kwargs):
        self.modbus.write_register(*args, **kwargs)

    def write_registers(self, *args, **kwargs):
        self.modbus.write_registers(*args, **kwargs)

    def _check_input_range(self, input_no):
        if input_no < 1 or input_no > self.MAX_CHANNEL:
            raise ValueError(f"input_no should be in range 1-{self.MAX_CHANNEL}.")

    def get_input_type(self, input_no: int) -> int:
        self._check_input_range(input_no)

        return self.read_register(ADR_INPUT_1_TYPE  + input_no - 1)

    def set_input_type(self, input_no: int, input_type: int) -> None:
        self._check_input_range(input_no)
        if input_type < 0 or input_type > 8:
            raise ValueError("input_type should be in range 0-8.")

        self.write_register(ADR_INPUT_1_TYPE  + input_no - 1, input_type)

    def get_input_filter(self, input_no: int) -> int:
        self._check_input_range(input_no)

        return self.read_register(ADR_INPUT_1_FILTER  + input_no - 1)

    def set_input_filter(self, input_no: int, input_filter: int) -> None:
        self._check_input_range(input_no)
        if input_filter < 0 or input_filter > 6:
            raise ValueError("input_type should be in range 0-6.")

        self.write_register(ADR_INPUT_1_FILTER  + input_no - 1, input_filter)

    def get_channel_value(self, channel_no: int, return_float: bool = False) -> int:
        self._check_input_range(channel_no)

        if not return_float:
            return self.read_register(ADR_CH1_VAL + channel_no - 1)
        else:
            return self.read_registers(ADR_CH1_FLOAT + channel_no - 1, 2)

    def get_channels_value(self, start_channel_no=1, end_channel_no=MAX_CHANNEL, return_float: bool = False) -> int:
        self._check_input_range(start_channel_no)
        self._check_input_range(end_channel_no)

        if end_channel_no == start_channel_no:
            return self.get_channel_value(start_channel_no, return_float)
        elif start_channel_no > end_channel_no:
            temp = end_channel_no
            end_channel_no = start_channel_no
            start_channel_no = temp

        if not return_float:
            return self.read_registers(ADR_CH1_VAL + start_channel_no - 1,
                                       end_channel_no - start_channel_no + 1)
        else:
            return self.read_registers(ADR_CH1_FLOAT + start_channel_no - 1,
                                       2 * (end_channel_no - start_channel_no + 1))

    def get_status(self) -> int:
        return self.read_register(ADR_STATUS)

    def get_errors(self) -> int:
        return self.read_register(ADR_ERRORS)
